import React from 'react';
import {HashRouter, Route, Link} from 'react-router-dom';

const Home = () => <div><h1>Home</h1><Links /></div>;
const About = () => <div><h1>About</h1><Links /></div>;
const Contact = () => <div><h1>Contact</h1><Links /></div>;

const Links = () =>
    <nav>
        <Link to="/">Home<br /></Link>
        <Link to="/about">About<br /></Link>
        <Link to="/contact">Contact<br /></Link>
    </nav>;

class App extends React.Component {
    render() {
        return (
            <HashRouter>
                <div> 
                    <Route exact path="/" component={Home}></Route>
                    <Route path="/about" component={About}></Route>
                    <Route path="/contact" component={Contact}></Route>
                </div>
            </HashRouter>
        );
    }
}

export default App;